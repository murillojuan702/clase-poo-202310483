<?php

abstract class Transporte{  //creamos la calse abstracta 

    abstract public function Mantenimiento();// creamos un metodo llamado mantenimiento con su modificador de acceso
    // este metodo se declara, pero no se implementa, no tien funcion en si, pero obligana que cualquier clase que contenga herencia exista

}

class Avion extends Transporte{ // creamos una clase erencia que implemente la clase Transporte

    public function Mantenimiento(){
        echo "AVION <br>";
        echo "revicion de alas <br> <br>";
    }

}

class Automovil extends Transporte{

    public function Mantenimiento(){
        echo "AUTOMOVIL <br>";
        echo "revicion de motor <br> <br>";
    }
}

class Autobus extends Transporte{

    public function Mantenimiento(){
        echo "AUTOBUS <br>";
        echo "revicion de motor <br>";
        echo "revicion de temperatura <br>";
        echo "revicion de disel <br>";
        echo "revicion de baterias <br>";
        echo "revicion de bujias <br>";
        echo "calibracion de llantas <br>";
        echo "revicion de limpieza <br>";
        echo "revicion de asientos <br> <br>"; 

    }
}

class Metro extends Transporte{

    public function Mantenimiento(){
        echo "METRO <br>";
        echo "revicion de motor <br>";
        echo "REVICION DE LA ESTRUCTRA DE RUTA <br>";
        echo "revicion de vagones <br>";
        echo "chequeo de controles de maquinaria <br>";
        echo "revicion de frenos <br> <br>";
    }
}

class Bicicleta extends Transporte{

    public function Mantenimiento(){
        echo "BICICLETA <BR>";
        echo "revicion de aciento <br>";
        echo "revicion de cadena <br>";
        echo "revicion de pedales <br>";
        echo "revicion de llantas <br>";
        echo "revicion de frenos <br>";
    }
}

$obj = new Avion();
$obj->Mantenimiento();

$obj2 = new Automovil();
$obj2->Mantenimiento();

$obj3 = new Autobus();
$obj3->Mantenimiento();

$obj4 = new Metro();
$obj4->Mantenimiento();

$obj5 = new Bicicleta();
$obj5->Mantenimiento();




?>