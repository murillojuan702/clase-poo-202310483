<?php
 //CREAMOS LA CLASE LLAMADA USUARIO
 class Usuario{
//damos los atributos de la classe  usuario
    public $nombre;
    public $correo;
    private $contraseña;
    protected $fecha_registro;
//ponemos nuestro constructor con parametros 
 public function __construct($nombre, $correo ){
    $this->nombre=$nombre;
    $this->correo=$correo;
    // AQUI USAMOS EL COMANDO RAND PARA GENERAR UNA CONTRASEÑA DIFERENTE CADA VEZ QUE SE REPITA EL CODIGO DE CONTRASEÑA
    $this->contraseña= rand();
    //AQUI AGREGAMOS UN COMANDO LLAMADO DATE PARA PONER LA FECHA ESACTA DE REGISTRO
    $this->fecha_registro = date('Y-m-d H:m:s');
}

//metodos de la clsse usuario
    public function Perfil(){
        echo "Datos del usuario <br>";
        echo "Nombre: ". $this->nombre."<br>";
        echo "Email: ". $this->correo."<br>";
        echo "Contraseña: ". $this->contraseña."<br>";
        echo "Fecha de registro: ". $this->fecha_registro."<br>";
        echo "<br><br>";
        echo "ASI SE MUESTRA EL RESULTADO DEL CODIGO EN PANTALLA ";
    }
   }
//LOS ATRIVUTOS CON LA MODIFICACION DE ACCESO "PRIVATE" SE PEUDEN LLAMAR MIENTRAS ESTEN EN LA MISMA CLASE, SI SE PONEN EN DIFERENTES 
//CLASSES O FUEREA DE LA QUE SE LES MENCIONA ESTA NO SE MOSTRARA EN PANTALLA POR MAS QUE LA MANDEN A LLAMAR.
// LAS DECLARACIONES DE TIPO PROTECTED TAMPOCO SE PUEDEN MOSTRAR EN PANTALLA FUERA DE LA CLASE, TIENE QUE SER LLAMADA DESDE LA CLASE
//EN LA QUE SE LE DECLARO, ESTA MISMA PUEDE SER LLAMADA CON LA HERENCIA YA QUE SON PARTE DE LA CLASE Y TIENEN PERMISO DE MODIFIAR
//LOS MODIFICADORES DE ACCESO PUBLIC PUEDEN SER LLAMDOS DE CUALQUIER PARTE DEL CODIGO, SIEMPRE Y CUANDO CONTENGA ALGO PARA MOSTRAR





?>