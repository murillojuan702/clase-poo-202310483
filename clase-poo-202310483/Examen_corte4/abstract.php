<?php

abstract class Animal{
    public $nombre;
    public $color;

    public function describir(){
        return "Elnombre del animal es : ".$this->nombre. ", el color del animanal es: ".$this->color;

    }

    abstract public function Sonido();

}

class Gato extends Animal{
    
    public function decribir(){
        return parent::describir();
    }
    public function sonido(){
        return "miau <br>";
    }
    
}
class Perro extends Animal{
    
    public function decribir(){
        return parent::describir();
    }
    public function sonido(){
        return  "Guau <br>";
    }
    
}



$obj = new Gato();
$obj->nombre = 'zeus';
$obj->color = 'Negro';
echo $obj->describir();
echo "<br>";
echo "el sonido que hace es: ".$obj->sonido();

$obj2 = new Perro();
$obj2->nombre = 'Hercules';
$obj2->color = 'Blanco';
echo $obj2->describir();
echo "<br>";
echo "el sonido que hace es: ".$obj2->sonido();

?>